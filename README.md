# googleMapsExample

Creando una aplicación con Google Maps:
Para crear tu primera aplicación de android con un mapa, recuerda lo siguiente:

1.- Entrar al SDK de android y descargar el "GOOGLE PLAY SERVICES" de lo contrario no funcionara.
2.- Implementa la dependencia de google play services gms.
3.- Crea un API KEY en la google console api para poder obtener acesso al mapa de google.

Creacion de gogle maps en nuestra aplicación:

En tu activity_main o el nombre que le hayas asignado a tu layout principal, deberas crear un , donde asginaras un ID para darle una asignacion de MAPAS.
Crearemos una nueva clase en nuestra carpeta JAVA con el nombre de MapFragment donde extenderemos de un SupporMapFragment, en donde inflaremos una nueva vista.

@Override public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) { View view = super.onCreateView(layoutInflater, viewGroup, bundle); return view; }

En nuestra Actividad principal iniciaeremos nuestra segunda Actividad: MapFragment (O el nombre que se le haya sido asignado), le daremos un identificador para poder hacer uso del mismo:

private MapFragment mapFragment;

Hacemos una nueva instancia con nuestro identificador el cual agregaremos la vista de nuestro mapa con el ID que se le fue asignado a nuestro contenedor , seguido implementaremos en nuestra clase principal un OnMapReadyCallBack para poder hacer uso interactivo de nuestro mapa.

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback

No olvides crear el escucha para el OnMapReadyCallback: mapFragment.getMapAsync(this);

*OnMapReadyCallback

Agregaremos una nueva posicion con su respectivo marcador y titulo para su identificación:

LatLng position = new LatLng(19.43001, -99.134617); googleMap.addMarker(new MarkerOptions() .position(position) .title("Titulo del marcador"));

!Listo, ya tienes tu primera aplicación de GoogleMaps en android, recuerda mirar el código si encuentras muy confusa la explicación y no olvides agregar la api_key, en mi caso agregue un string y en el meta-data del manifest solo le pase el string para un poco de profesionalismo pero puedes realizarlo directo, tambien no olvides agregar los permisos en tu archivo manifest!
